DROP TABLE IF EXISTS `{section}_source`;
CREATE TABLE `{section}_source` (
	`id`    INT(11)       NOT NULL AUTO_INCREMENT,
	`title` VARCHAR(250)  NOT NULL,
	`root`  VARCHAR(1000) NOT NULL,

	PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

INSERT INTO `{section}_source` (`title`, `root`) VALUES ('Pharmacosmetica', 'https://www.pharmacosmetica.ru');

DROP TABLE IF EXISTS `{section}_sync`;
CREATE TABLE `{section}_sync` (
	`id`          INT(11) NOT NULL AUTO_INCREMENT,
	`source_id`   INT(11) NOT NULL,
	`started_at`  INT(11) NOT NULL,
	`finished_at` INT(11),
	`stats_json`  TEXT,

	PRIMARY KEY (`id`),
	KEY         (`source_id`),
	KEY         (`started_at`),
	KEY         (`finished_at`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{section}_product`;
CREATE TABLE `{section}_product` (
	`id`        INT(11)       NOT NULL AUTO_INCREMENT,
	`source_id` INT(11)       NOT NULL,
	`article`   VARCHAR(50)   NOT NULL,
	`url`       VARCHAR(1000),

	PRIMARY KEY (`id`),
	UNIQUE      (`source_id`, `article`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `{section}_comment`;
CREATE TABLE `{section}_comment` (
	`id`              INT(11)       NOT NULL AUTO_INCREMENT,
	`source_id`       INT(11)       NOT NULL,
	`product_article` VARCHAR(50)   NOT NULL,
	`author_name`     VARCHAR(250),
	`timestamp`       INT(11),
	`message_json`    TEXT,
	`stars`           TINYINT(1),
	`image_json`      TEXT,

	PRIMARY KEY (`id`),
	KEY         (`source_id`),
	KEY         (`product_article`),
	KEY         (`timestamp`),
	KEY         (`stars`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
