<?php

class ExtComments extends A_MainFrame {

	function Action ($action) {
		$this->data = array();
		switch ( $action ) {
			case 'getSources':  $this->getSources();  return;
			case 'getSyncs':    $this->getSyncs();    return;
			case 'getComments': $this->getComments(); return;
		}
		A::NotFound();
	}

	function display () {
		header('Content-Type: application/json');
		echo json_encode($this->data);
	}

	private static $SOURCE_FIELD_CONV = array('id'=>'int', 'title'=>'str', 'root'=>'skip');

	private function getSources () {
		$sources = array();
		$source_rows = A::$DB->getAll('SELECT * FROM '.SECTION.'_source');
		foreach ( $source_rows as $source_row ) $sources[] = self::convertRow($source_row, self::$SOURCE_FIELD_CONV);

		$this->data['sources'] = $sources;
	}

	private static $SYNC_FIELD_CONV = array('id'=>'skip', 'source_id'=>'int', 'started_at'=>'ts', 'finished_at'=>'ts', 'stats_json'=>'json stats');

	private function getSyncs () {
		$syncs = array();
		$sync_rows = A::$DB->getAll('SELECT * FROM '.SECTION.'_sync');
		foreach ( $sync_rows as $sync_row ) $syncs[] = self::convertRow($sync_row, self::$SYNC_FIELD_CONV);

		$this->data['syncs'] = $syncs;
	}

	private static $PRODUCT_FIELD_CONV = array('id'=>'skip', 'source_id'=>'int', 'article'=>'str', 'url'=>'str');
	private static $COMMENT_FIELD_CONV = array('id'=>'skip', 'source_id'=>'int', 'product_article'=>'str', 'author_name'=>'str', 'timestamp'=>'ts', 'message_json'=>'special_message message_html', 'stars'=>'int', 'image_json'=>'json image');

	private function getComments () {
		if ( !isset($_GET['forProductArticle']) ) return A::NotFound();
		$product_article = $_GET['forProductArticle'];
		$comments_where_and = array('product_article = ?');
		$comments_params = array($product_article);
		$products_params = array($product_article);

		if ( isset($_GET['forSourceIds']) ) {
			$source_count = 0;
			foreach ( explode(',', $_GET['forSourceIds']) as $source_id ) {
				$comments_params[] = $source_id;
				++$source_count;
			}
			if ( $source_count > 0 )
				$comments_where_and[] = 'source_id IN ('. implode(', ', array_fill(0, $source_count, '?')) .')';
		}

		$comments_sql = 'SELECT * FROM '.SECTION.'_comment WHERE '. implode(' AND ', $comments_where_and) .' ORDER BY timestamp DESC';
		$products_sql = 'SELECT * FROM '.SECTION.'_product WHERE article = ?'; //TODO only those whose comments are output

		$comments = array();
		$comment_rows = A::$DB->getAll($comments_sql, $comments_params);
		foreach ( $comment_rows as $comment_row ) $comments[] = self::convertRow($comment_row, self::$COMMENT_FIELD_CONV);

		$productsBySourceId = array();
		$product_rows = A::$DB->getAll($products_sql, $products_params);
		foreach ( $product_rows as $product_row ) {
			$source_id = $product_row['source_id'];
			$productsBySourceId[$source_id] = self::convertRow($product_row, self::$PRODUCT_FIELD_CONV);
		}

		$this->data['comments'] = $comments;
		$this->data['productsBySourceId'] = $productsBySourceId;
	}

	private static function convertRow ($row, $conv) {
		$hash = array();
		foreach ( $conv as $rname => $rule ) {
			$rule_array = preg_split('/\s+/', $rule);
			if ( count($rule_array) == 2 ) list($rule, $hname) = $rule_array; else $hname = $rname;
			switch ( $rule ) {
				case 'str':
					$hash[$hname] = $row[$rname];
					break;
				case 'int':
				case 'ts':
					$hash[$hname] = intval($row[$rname]);
					break;
				case 'json':
					$hash[$hname] = json_decode($row[$rname]);
					break;
				case 'special_message':
					$hash[$hname] = self::convertMessageField($row[$rname]);
					break;
				case 'skip':
				default:
			}
		}
		return $hash;
	}

	private static $MESSAGE_JSON_CONV = array('eff_text'=>'Эффективность', 'tex_text'=>'Текстура', 'eco_text'=>'Расход');

	private static function convertMessageField ($message_json) {
		$message_html = '';
		$use_sections = FALSE;
		$message_data = json_decode($message_json);
		foreach ( self::$MESSAGE_JSON_CONV as $field => $title ) {
			if ( empty($message_data->$field) ) continue;
			$message_html .= "<h6>{$title}</h6><p>{$message_data->$field}</p>";
			$use_sections = TRUE;
		}
		if ( $use_sections ) $message_html .= "<h6>Выводы</h6>";
		$message_html .= "<p>{$message_data->gen_html}<p>";
		return $message_html;
	}

}
A::$MAINFRAME = new ExtComments;
