<?php

$sources = A::$DB->getAll('SELECT * FROM '.A::$SECTION.'_source');
foreach ( $sources as $source ) sync_source($source);
echo("DONE\n"); //XXX

function sync_source ($source) {
	$stats = array('products_count'=>0, 'comments_count'=>0); //TODO failed_requests_count failed_inserts_count
	$sync_id = A::$DB->Insert(A::$SECTION.'_sync', array('source_id'=>$source['id'], 'started_at'=>time()));
	sync_products($source, $stats);
	A::$DB->Update(A::$SECTION.'_sync', array('finished_at'=>time(), 'stats_json'=>json_encode($stats)), 'id = ?', array($sync_id));
}

function sync_products ($source, &$stats) {
	A::$DB->Delete(A::$SECTION.'_product', 'source_id = ?', array($source['id']));
	$products = array();
	for ( $products_page = $products_last_page = 1; $products_page <= $products_last_page; ++$products_page ) {
		$products_json_url = "{$source['root']}/cosmedel/?action=getProducts&block=eshop&page={$products_page}&limit=2000";
		echo("$products_json_url\n"); //XXX
		$products_json = file_get_contents($products_json_url);
		$products_data = json_decode($products_json);
		foreach ( $products_data->product as $product ) {
			$product = array(
				'source_id'=>$source['id'],
				'article'  =>$product->art,
				'url'      =>$product->url,
			);
			A::$DB->Insert(A::$SECTION.'_product', $product);
			$products[] = $product;
			++$stats['products_count'];
		}
		$products_last_page = intval($products_data->results->all_pages);
	}
	sync_comments($source, $products, $stats);
}

function sync_comments ($source, $products, &$stats) {
	A::$DB->Delete(A::$SECTION.'_comment', 'source_id = ?', array($source['id']));
	foreach ( $products as $product ) {
		$comments_json_url = "{$source['root']}/cosmedel/?action=getComments&block=eshop&art={$product['article']}";
		echo("$comments_json_url\n"); //XXX
		$comments_json = file_get_contents($comments_json_url);
		$comments_data = json_decode($comments_json);
		foreach ( $comments_data->comment as $comment_data ) { //TODO check if ->comment exists (i.e. on 'GW1*40105')
			$comment = array(
				'source_id'      =>$source['id'],
				'product_article'=>$product['article'],
				'author_name'    =>$comment_data->name,
				'timestamp'      =>$comment_data->date,
				'message_json'   =>json_encode(array(
					'eff_text'=>$comment_data->message1, // Эффективность
					'tex_text'=>$comment_data->message2, // Текстура
					'eco_text'=>$comment_data->message3, // Расход
					'gen_html'=>$comment_data->message,  // Выводы
				)),
				'stars'          =>$comment_data->vote,
			);
			if ( !empty($comment_data->images) ) {
				$comment_image_data = $comment_data->images[0];
				$comment['image_json'] = json_encode(array(
					'url'   =>"{$source['root']}/{$comment_image_data->path}",
					'width' =>intval($comment_image_data->width),
					'height'=>intval($comment_image_data->height),
				));
			}
			A::$DB->Insert(A::$SECTION.'_comment', $comment);
			++$stats['comments_count'];
		}
	}
}
